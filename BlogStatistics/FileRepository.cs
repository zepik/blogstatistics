﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BlogStatistics
{
    class FileRepository
    {
        public void SaveDictionaryContentToTextFile(Dictionary<char, int> dataToSave)
        {
            string directoryToSavePath = CreateDirectoryIfDoesNotExist();

            DeleteUnnecessaryFiles(directoryToSavePath);

            using (StreamWriter sw = new StreamWriter(CreateFileToSaveResults(directoryToSavePath)))
            {
                foreach (string dataLine in FormatDataToWrite(dataToSave))
                {
                    sw.WriteLine(dataLine);
                }
            }

            Console.WriteLine($"File saved in folder: {directoryToSavePath}");
        }

        private void DeleteUnnecessaryFiles(string directoryToSavePath)
        {
            var filesToDelete = new DirectoryInfo(directoryToSavePath).EnumerateFiles()
                 .OrderByDescending(f => f.CreationTime)
                 .Skip(5)
                 .ToList();

            filesToDelete.ForEach(f => f.Delete());
        }

        private static IEnumerable<string> FormatDataToWrite(Dictionary<char, int> dataToSave)
        {
            return dataToSave.Select(d => $"{d.Key} ({(int)d.Key}) - {d.Value}");
        }

        private static string CreateFileToSaveResults(string directoryToSavePath)
        {
            var filePath = Path.Combine(directoryToSavePath, $"TextStatistics_{DateTime.Now:yyyy-MM-dd HH_mm_ss}");
            return Path.ChangeExtension(filePath, ".txt");
        }

        private static string CreateDirectoryIfDoesNotExist()
        {
            var directoryToSavePath = Path.Combine(Environment.CurrentDirectory, "Results");
            Directory.CreateDirectory(directoryToSavePath);
            return directoryToSavePath;
        }
    }
}
