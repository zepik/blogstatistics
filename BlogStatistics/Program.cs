﻿using System;
using System.Timers;

namespace BlogStatistics
{
    class Program
    {
        private static readonly FinAiBlogDownloader BlogDownloader = new FinAiBlogDownloader();
        private static readonly TextAnalyzer TextAnalyzer = new TextAnalyzer();
        private static readonly FileRepository FileRepository = new FileRepository();

        static void Main(string[] args)
        {
            var scheduler = new Scheduler();
            scheduler.Start();
            scheduler.AddEvent(HandleTimerElapsed);
            Console.WriteLine("Press a key to turn off app.");
            Console.ReadKey();
            scheduler.Stop();
        }

        public static void HandleTimerElapsed(object sender, ElapsedEventArgs e)
        {
            var headersText = BlogDownloader.GetFirstTwentyPostsHeaders();
            var result = TextAnalyzer.GetNumberOfLettersInText(headersText);
            FileRepository.SaveDictionaryContentToTextFile(result);
        }
    }
}
