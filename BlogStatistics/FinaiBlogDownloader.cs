﻿using HtmlAgilityPack;
using System.Text;

namespace BlogStatistics
{
    class FinAiBlogDownloader
    {
        private readonly HtmlWeb _web;

        public FinAiBlogDownloader()
        {
            _web = new HtmlWeb {OverrideEncoding = Encoding.UTF8};
        }

        public string GetFirstTwentyPostsHeaders()
        {
            int numberOfDownloadedPosts = 0;
            int pageNumber = 1;
            var result = new StringBuilder();

            do
            {
                var posts = GetAllPostsFromPage($"https://www.finai.pl/blog?page={pageNumber}");
             
                foreach (var post in posts)
                {
                    result.Append(post.InnerText.Trim());
                    if (++numberOfDownloadedPosts >= 20)
                        break;
                }
                pageNumber++;
            } while (numberOfDownloadedPosts < 20);

            return result.ToString();
        }

        private HtmlNodeCollection GetAllPostsFromPage(string url)
        {
            var htmlDoc = _web.Load(url);
            return  htmlDoc.DocumentNode
                .SelectNodes(@"//*[contains(@class,'blog-post-list')]
                    //ul
                    //*[contains(@class,'post')]
                    //*[contains(@class,'post-content')]
                    //*[contains(@class,'post-entry')]
                    //*[contains(@class,'post-title')]");
        }
    }
}
