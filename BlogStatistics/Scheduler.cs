﻿using System;
using System.Timers;

namespace BlogStatistics
{
    class Scheduler
    {
        private Timer _timer;

        public void Start()
        {
            _timer = new Timer();
            _timer.Elapsed += OnTimeEvent;
            _timer.Interval = 10 * 60 * 1000;
            _timer.Enabled = true;
        }

        public void AddEvent(ElapsedEventHandler handler)
        {
            _timer.Elapsed += handler;
        }

        public void Stop()
        {
            _timer.Stop();
        }

        private void OnTimeEvent(object oSource, ElapsedEventArgs oElapsedEventArgs)
        {
            Console.WriteLine("Time event occured");
        }
    }
}
