﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlogStatistics
{
    class TextAnalyzer
    {
        public Dictionary<char, int> GetNumberOfLettersInText(string textToAnalyze)
        {
            textToAnalyze = textToAnalyze.Replace("&#243;", "ó");
            return textToAnalyze
                .Where(char.IsLetter)
                .GroupBy(t => char.ToLower(t))
                .OrderByDescending(r => r.Count())
                .ToDictionary(g => g.Key, g => g.Count());
        }
    }
}
